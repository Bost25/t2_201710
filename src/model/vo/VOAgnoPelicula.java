package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;

public class VOAgnoPelicula {

	private int agno;
	private ListaEncadenada<VOPelicula> peliculas;

	public VOAgnoPelicula()
	{
		peliculas = new ListaEncadenada<VOPelicula>();
	}
	
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(ListaEncadenada<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}
	public void  agregarNuevaPelicula(VOPelicula pPelicula){
		peliculas.agregarElementoFinal(pPelicula);
	}
	
}
