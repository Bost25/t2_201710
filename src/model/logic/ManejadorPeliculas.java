package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaEncadenada<VOAgnoPelicula>();

		BufferedReader br;

		try {

			br=new BufferedReader(new FileReader("data/movies.csv"));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				misPeliculas.agregarElementoFinal(nuevaPelicula(line));
				line = br.readLine();
			}

			movieSort();
			System.out.println("SE CARGARON LAS PELICULAS");
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}

	}

	private VOPelicula nuevaPelicula(String a)
	{

		VOPelicula film = new VOPelicula();


		int last = a.lastIndexOf(",");

		String com = a.substring(last+1);

		ListaEncadenada<String> gen = new ListaEncadenada<>();

		String[] generos =com.split("\\|");

		for(String genero : generos){
			gen.agregarElementoFinal(genero);
		}


		film.setGenerosAsociados(gen);
		int first = a.indexOf(",");
		String nom;

		if (a.contains("\"")){
			nom = (String) a.subSequence(first+1, last);
		}
		else {
			nom = (String) a.subSequence(first+1, last-6);

		}
		film.setTitulo(nom);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String sinGeneros = (String) a.subSequence(0, last);
		int p = sinGeneros.lastIndexOf("(");
		if(p != -1){
			String anio = sinGeneros.replaceAll("\""," " ).trim().substring(p+1,p+5);

			film.setAgnoPublicacion(Integer.parseInt(anio));
		}

		else{
			film.setAgnoPublicacion(0000);
		}
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		boolean hay = false;
		for(int i = 0; i < peliculasAgno.darNumeroElementos(); i++){
			VOAgnoPelicula pel = peliculasAgno.darElemento(i);
			int fecha = pel.getAgno();
			if(fecha == film.getAgnoPublicacion())
			{
				pel.agregarNuevaPelicula(film);
				hay = true;
			}
		}
		if(hay == false)
		{
			VOAgnoPelicula var = new VOAgnoPelicula();
			var.setAgno(film.getAgnoPublicacion());
			var.agregarNuevaPelicula(film);
			peliculasAgno.agregarElementoFinal(var);
		}


		System.out.println("Nombre:"+film.getTitulo() +" a�o: " + film.getAgnoPublicacion() +" generos: "+ Arrays.toString(generos));
		return film;

	}

	private void movieSort ()
	{
		VOAgnoPelicula [] temp = new VOAgnoPelicula [peliculasAgno.darNumeroElementos()];

		for (int i = 0; i < peliculasAgno.darNumeroElementos(); i++) {
			temp[i] = peliculasAgno.darElemento(i);
		}

		int N = temp.length;
		for (int i = 1; i < N; i++) {
			VOAgnoPelicula porIns = temp[i];
			boolean termino = false;
			for (int j = i; j >0 && !termino; j--) {
				VOAgnoPelicula actual = temp[j-1];
				if(actual.getAgno() > porIns.getAgno())
				{
					temp[j] = actual;
					temp[j-1] = porIns;
				}else
				{
					termino = true;
				}
			}
		}
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		for (int i = 0; i < temp.length; i++) {
			peliculasAgno.agregarElementoFinal(temp[i]);
		}
	}


	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ListaEncadenada<VOPelicula> var = new ListaEncadenada<VOPelicula>();

		for (int i = 0; i < misPeliculas.darNumeroElementos(); i++){
			VOPelicula pel = misPeliculas.darElemento(i);
			if(pel.getTitulo().contains(busqueda)){
				var.agregarElementoFinal(pel);
			}
		}

		return var;
	}



	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		int cont = 0;
		for(int i = 0; i < peliculasAgno.darNumeroElementos(); i++){
			VOAgnoPelicula pel = peliculasAgno.darElemento(i);
			if(pel.getAgno() == agno){
				peliculasAgno.darElemento(cont);
				return pel.getPeliculas();
			}
			cont ++;
		}
		return null;
	}



	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {

		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();

	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {

		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}






}
