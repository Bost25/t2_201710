package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> actual;

	private NodoSencillo<T> primero;

	public ListaEncadenada(){
		primero = null;
		actual = primero;
	}

	@Override
	public Iterator<T> iterator() {			
		// TODO Auto-generated method stub
		return this.actual;
	}


	public boolean estaVacia(){
		return primero == null;
	}


	public void agregarElementoFinal(T elem) {
		NodoSencillo<T> var = new NodoSencillo<>(elem);
		NodoSencillo<T> otro;
		if(darNumeroElementos() == 0){
			primero = var;
			actual = var;
		}
		else{
			otro = actual;
			while(otro.hasNext()){
				otro = otro.next();
			}
			otro.setSiguiente(var);
		}
		// TODO Auto-generated method stub

	}

	@Override
	public T darElemento(int pos) {
		int c=0;
		NodoSencillo<T> r = primero;
		T tipo = null;
		if(pos < 0 || pos >= darNumeroElementos()){
			System.out.println("La posicion requerida no existe");
		}
		else{
			while(r != null){
				if(c == pos){
					tipo = r.getTipo();
					actual = r;
					break;
				}
				else{
					r = r.next();
					c++;
				}
			}
		}
		return tipo;
		// TODO Auto-generated method stub
	}


	@Override
	public int darNumeroElementos() {
		NodoSencillo<T> var = primero;
		int c=0;
		if(estaVacia()){
			c = 0;
		}
		else{
			while(var != null){
				c++;
				var = var.next();
			}
		}
		return c;
		// TODO Auto-generated method stub
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.getTipo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean r= false;
		if(actual.hasNext()){
			actual = actual.next();
			r = true;
		}
		return r;
	}


	@Override
	public boolean retrocederPosicionAnterior() {
		boolean r = false;
		NodoSencillo<T> var = primero;
		// TODO Auto-generated method stub
		if(actual == primero){
			r = false;
		}
		else{

			while(var != null){
				if(var.next() == actual){
					actual = var;
					r = true;
					break;
				}
				else{
					var = var.next();
				}
			}
		}
		return r;
	}
	
	
}
