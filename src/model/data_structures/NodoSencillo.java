package model.data_structures;

import java.util.Iterator;

public class NodoSencillo<T> implements Iterator{
	
	private T tipo;
	
	private NodoSencillo<T> siguiente; 
	
	
	public NodoSencillo() {
		siguiente = null;
		// TODO Auto-generated constructor stub
	}
	
	public NodoSencillo(T p){
        siguiente=null;
        tipo = p;
    }
 
	public NodoSencillo(T t, NodoSencillo<T> siguiente){
        this.siguiente=siguiente;
        tipo = t;
    }

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return siguiente != null;
	}

	@Override
	public NodoSencillo<T> next() {
		// TODO Auto-generated method stub
		return siguiente;
	}

	public T getTipo(){
		return tipo;
	}
	
	public void setTipo(T tipo) {
        this.tipo = tipo;
    }
	
	public void setSiguiente(NodoSencillo<T> siguiente) {
        this.siguiente = siguiente;
    }

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

}
