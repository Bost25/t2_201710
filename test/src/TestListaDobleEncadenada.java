import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;


public class TestListaDobleEncadenada extends TestCase {

	
	private ListaDobleEncadenada<Number> list;
	
	public void setupEscenario1(){
		list = new ListaDobleEncadenada<Number>();
		list.agregarElementoFinal(1);
		list.agregarElementoFinal(16);
		list.agregarElementoFinal(15);
		list.agregarElementoFinal(2);
		list.agregarElementoFinal(86);
	} 
	
	public void testAgregarElementoFinalYDarElemento(){
		
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 1, list.darElemento(0));
		assertEquals("No es el numero esperado", 16, list.darElemento(1));
		assertEquals("No es el numero esperado", 15, list.darElemento(2));
		assertEquals("No es el numero esperado", 2, list.darElemento(3));
		assertEquals("No es el numero esperado", 86, list.darElemento(4));
		
	}
	
	public void testDarNumeroElementos(){
		
		setupEscenario1();
		
		assertEquals("No es la cantidad esperada", 5, list.darNumeroElementos());
		
		list.agregarElementoFinal(76);
		assertEquals("No es la cantidad esperada", 6, list.darNumeroElementos());
		
	}
	
	public void testDarElementoPosicionActual(){
		
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 1, list.darElementoPosicionActual());
		
		list.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 16, list.darElementoPosicionActual());
		
		list.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 15, list.darElementoPosicionActual());
		
		list.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 2, list.darElementoPosicionActual());
	}
	
	public void testAvanzarPosicion(){
		
		setupEscenario1();
		
		assertEquals("No es lo esperado", true, list.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 16, list.darElementoPosicionActual());
		
		assertEquals("No es lo esperado", true, list.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 15, list.darElementoPosicionActual());
		
		for(int i = 0; i<4; i++){
			list.avanzarSiguientePosicion();
		}
		
		assertEquals("No es lo esperado", false, list.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 86, list.darElementoPosicionActual());
		
	}

	public void testRetrocederPosicion(){
		
		setupEscenario1();
		
		assertEquals("No es lo esperado", false, list.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 1, list.darElementoPosicionActual());
		
		for(int i = 0; i<5; i++){
			list.avanzarSiguientePosicion();
		}
		
		assertEquals("No es lo esperado", true, list.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 2, list.darElementoPosicionActual());
		
		assertEquals("No es lo esperado", true, list.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 15, list.darElementoPosicionActual());
		
		
		
	}
}
